<?php

use Skeletor\Skeletor;
use Classes\AutoPyTorrent\AutoPyTorrent;
use Classes\Transmission\Transmission;

class init
{
    protected $get = [];
    protected $post = [];
    protected $cli = [];

    protected $request = [];

    /**
     * init constructor.
     */
    public function __construct()
    {
        if (isset($_GET))
            foreach ($_GET as $key => $value) {
                $this->get[$key] = $value;
            }

        if (isset($_POST))
            foreach ($_POST as $key => $value) {
                $this->post[$key] = $value;
            }

        if (isset($argv))
            foreach ($argv as $key => $value) {
                $this->cli[$key] = $value;
            }

        $this->request = array_merge($this->get, $this->post, $this->cli);
    }

    /**
     * @return array
     */
    public function run()
    {
        require_once './../Skeletor/Skeletor.php';

        $skeletor = new Skeletor();
        $skeletor->debugger(true);

        if(empty($_REQUEST['q']))
        	exit;


        $link = (new AutoPyTorrent())
            ->search($_REQUEST['q'],((isset($_REQUEST['m'])&&((boolean)$_REQUEST['m'])===true)));

        if (!$link)
            return ['result' => false];

        return ['result'=>(new Transmission())
            ->queue($link)];
    }
}

$init = new init();

header('Content-Type: application/json');
echo json_encode($init->run());
exit;
