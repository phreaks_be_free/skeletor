<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 1/9/18
 * Time: 3:43 PM
 */
namespace System\Database\Eloquent;

require_once __FRAME_ROOT__.'/vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;

class Elequent
{
	private $capsule;
	/**
	 * Elequent constructor.
	 * @throws \Exception
	 */
	public function __construct()
	{

		$config = json_decode(include('../config.json'));

		if(empty($config))
			throw new \Exception('Invalid Database Configuration');

		$this->capsule = new Capsule;
		$this->capsule->addConnection($config);


	}

	/**
	 * @return void
	 */
	public function run(){
		$this->capsule->bootEloquent();
	}
}