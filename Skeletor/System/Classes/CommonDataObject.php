<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 11/28/17
 * Time: 12:31 PM
 */

namespace System\Classes;


class CommonDataObject
{
    /** @var string  */
    protected $datetimeOutputFormat = 'Y-m-d H:i:s';

    /** @var array  */
    protected $ignoreVariables = [
        'connectService',
        'ds',
        'ignoreVariables',
        'datetimeOutputFormat',
        'linkedValues'
    ];

    /** @var array ['id'=>'productId','productId'=>'id'] */
    protected $linkedValues = [];

    /**
     * CommonDataObject constructor.
     * @param $product
     * @throws \Exception
     */
    public function __construct( $product)
    {
        $this->populateByKeyPair($product);
    }

    /**
     * @param $product
     * @return $this
     * @throws \Exception
     */
    public function populateByKeyPair($product)
    {
        if (!is_array($product) && !is_object($product))
            throw new \Exception('Invalid Key Pair');

        /**
         * Define Product
         */
        foreach ($product as $key => $property) {
            if (empty($property) && $property !== 0)
                continue;

            if (in_array($key, $this->ignoreVariables))
                continue;

            /**
             * update property
             */
            if (property_exists($this, $key))
                $this->$key = $property;

            /**
             * update linked property
             */
            if (isset($this->linkedValues[$key])) {
                $link = $this->linkedValues[$key];
                if (property_exists($this, $link))
                    $this->$link = $property;
            }
        }
        return $this;
    }

    /**
     * @param $column
     * @return null
     */
    public function _get($column)
    {
        foreach (get_object_vars($this) as $name => $var) {
            if (in_array($column, $this->ignoreVariables))
                continue;

            if ($name === $column)
                return $var;
        }
        return $this;
    }

    /**
     * @param $column
     * @param $value
     * @return null
     */
    public function _set($column, $value)
    {
        foreach (get_object_vars($this) as $name => $var) {
            if (in_array($name, $this->ignoreVariables))
                continue;

            if ($name === $column)
                $this->$column = $value;
        }
        return $this;
    }

    /**
     * @return array
     */
    public function output()
    {
        $output = [];

        foreach (get_object_vars($this) as $name => $var) {
            if (in_array($name, $this->ignoreVariables))
                continue;

            $output[$name] = $var;
        }
        return $output;
    }
}

