<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 5/17/2017
 * Time: 4:38 AM
 */

namespace System\Classes;



abstract class CommonObjectModel
{
    protected $database_model;
    private $modelName;
    protected $datetimeOutputFormat = 'Y-m-d H:i:s';

    abstract public function save();

    /**
     * CommonObjectModel constructor.
     * @param DoctrineServiceFactory $doctrineServiceFactory
     * @param $modelName
     * @param null $PK
     * @param null $onLoadCallback
     * @throws \Exception
     */
    public function __construct( $modelName, $PK = null, $onLoadCallback = null)
    {

        $this->modelName = '\Common\Model\\' . $modelName;

        $this->load(null, $PK, $onLoadCallback);

    }

    /**
     * @param $column
     * @param bool $strict
     * @return null
     */
    public function _get($column, $strict = false)
    {
        if (empty($this->database_model))
            return null;

        if ($strict)
            return $this->$column;
        else
            $function = 'get' . ucfirst(strtolower($column));

        return $this->database_model->$function();
    }

    /**
     * @param $column
     * @param $value
     * @return null
     */
    public function _set($column, $value)
    {
        if (empty($this->database_model))
            return null;

        $function = 'set' . ucfirst(strtolower($column));
        $this->database_model->$function($value);
        return true;
    }


    /**
     * Check if Model is populated before returning information;
     * @return bool
     */
    protected function checkModel()
    {
        if (empty($this->database_model))
            return false;
        return true;
    }

    /**
     * @param null $database_model
     * @param null $PK
     * @param null $callback
     * @return bool
     * @throws \Exception
     */
    protected function load($database_model = null, $PK = null, $callback = null)
    {
        if ($callback == null)
            $callback = function () {
                return true;
            };

        /**
         * Primary Empty Model
         */
        if (empty($database_model) && empty($PK)) {
            $this->database_model = new $this->modelName();
            return $callback(null, $PK);
        }

        /**
         * Load Database Model By PK
         */
        if (empty($database_model) && !empty($PK)) {
            if (is_a($PK, $this->modelName))
                $this->database_model = $PK;
            else
                $this->database_model = $this->ds->find($this->modelName, $PK);

            return $callback(null, $PK);
        }

        /**
         * Check for Database Model
         */
        if (!is_a($database_model, $this->modelName))
            return false;

        /**
         * Set Database Model
         */
        if (!empty($database_model)) {
            $this->database_model = $database_model;
            return $callback($database_model, $PK);
        }

        return false;
    }

    /**
     * Get Database Model
     * @return null
     */
    public function getDatabaseModel()
    {
        if (!$this->checkModel())
            return null;
        return $this->database_model;
    }

    /**
     * Get Database Model Id
     * @return null
     */
    public function getId()
    {
        if (!$this->checkModel())
            return null;
        return $this->database_model->getId();
    }
}