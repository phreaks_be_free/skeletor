<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 12/18/17
 * Time: 10:55 PM
 */

namespace System\Classes;


abstract class CommonStrictDataModel
{
    protected $options = [
        'option1',
        'option2',
        'option3'
    ];

    protected $selected = false;

    /**
     * TimeConflictOptions constructor.
     * @param null $option
     * @throws \Exception
     */
    public function __construct($option = null)
    {
        if (!empty($option))
            $this->select($option);
    }

    /**
     * @param $option
     * @return bool
     * @throws \Exception
     */
    public function select($option)
    {
        if (!is_string($option))
            throw new \Exception('Invalid Option Passed');

        foreach ($this->options as $key => $value) {
            if (strtolower($option) === strtolower($value))
                $this->selected = $key;
        }
        return true;
    }

    /**
     * @return bool|mixed
     */
    public function selected()
    {
        if (!$this->selected)
            return $this->selected;

        return $this->options[$this->selected];
    }

    /**
     * @return array
     */
    public function options()
    {
        return $this->options;
    }
}