<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 12/24/17
 * Time: 6:26 PM
 */

namespace System\Shell;

use System\Shell\Model\Command;

class Shell
{

	protected $cmd;

	/**
	 * Shell constructor.
	 * @param $cmd
	 */
	public function __construct($cmd = null)
	{
		if (!empty($cmd))
			$this->cmd = new Command($cmd);
	}

	/**
	 * @return string
	 */
	public function shell()
	{
		return shell_exec($this->cmd->run());
	}

	/**
	 * @return array
	 */
	public function streamExec()
	{
		$data = [];
		$descriptorspec = array(
			0 => array("pipe", "r"),   // stdin is a pipe that the child will read from
			1 => array("pipe", "w"),   // stdout is a pipe that the child will write to
			2 => array("pipe", "w")    // stderr is a pipe that the child will write to
		);

		flush();
		$process = proc_open($this->cmd->run(), $descriptorspec, $pipes, realpath('./'), array());

		if (is_resource($process)) {
			while ($s = fgets($pipes[1])) {
				$data[$this->cmd->run()] = $s;
				flush();
			}
		}
		return $data;
	}

	/**
	 * @param $cmd
	 * @return $this
	 */
	public function submitCommand($cmd)
	{
		$this->cmd = new Command($cmd);
		return $this;
	}
}