<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 12/24/17
 * Time: 6:35 PM
 */
namespace System\Shell\Model;

class Command
{
    protected $commandLine;

    /**
     * Command constructor.
     * @param $cmd
     * @throws \Exception
     */
    public function __construct($cmd)
    {
        if(!is_string($cmd))
            throw new \Exception('Invalid Command Line');

        $this->commandLine = $cmd;
    }

    /**
     * @return string
     */
    public function run(){
        return $this->commandLine;
    }
}