<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 12/22/17
 * Time: 6:12 AM
 */

namespace Skeletor\Logger;

class Logger
{
    public function __construct($category)
    {
        $found = false;
        foreach (scandir($_SERVER['DOCUMENT_ROOT'] . '../') as $path) {
            if ($path == 'logs')
                $found = true;
        }

        if (!$found)
            mkdir($_SERVER['DOCUMENT_ROOT'] . '../logs');

    }
}