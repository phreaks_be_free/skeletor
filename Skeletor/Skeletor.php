<?php

namespace Skeletor;

use Skeletor\Autoload\Autoloader;
use Skeletor\Logger\Logger;

class Skeletor
{
    private $logger;

    private $autoloader;

    protected $db;

    /**
     * Skeletor constructor.
     * @throws \Exception
     */
    public function __construct()
    {
    	define('__FRAME_ROOT__',  __DIR__);
    	define('__SYSTEM__',__DIR__.DIRECTORY_SEPARATOR.'System');


        require_once __DIR__ . DIRECTORY_SEPARATOR . 'Autoload' . DIRECTORY_SEPARATOR . 'Autoloader.php';

        $this->autoloader = new Autoloader();
        $this->autoloader->autoloadFolder(__DIR__.DIRECTORY_SEPARATOR.'Classes');

        $this->logger = new Logger('system');

        $this->db = new \System\Database\Eloquent\Elequent();

		//Composer Load Classes Folder Maybe
//		$loader = require __DIR__.'vendor/autoload.php';
//		$loader->add('Namespace\\Somewhere\\Else\\', __DIR__);
//		$loader->add('Namespace\\Somewhere\\Else2\\', '/var/www/html/xxx');
    }

    /**
     * @param $boolean
     * @return bool
     */
    public function debugger($boolean)
    {
        if (!is_bool($boolean))
            return false;

        ini_set('display_errors', $boolean);
        ini_set('display_startup_errors', $boolean);

        if ($boolean)
            error_reporting(E_ALL);
        else
            error_reporting(0);
    }
}