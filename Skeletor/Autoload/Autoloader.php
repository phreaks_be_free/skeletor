<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 12/22/17
 * Time: 6:16 AM
 */

namespace Skeletor\Autoload;


class Autoloader
{
	/** @var array */
	private $include;

	/**
	 * Autoloader constructor.
	 * @throws \Exception
	 */
	public function __construct()
	{
		$this->include = [];
		//$this->included = $this->recursiveInclude($this->recursiveSearch(__DIR__ . '/Vendor'));
	}

	/**
	 * @param $search
	 * @return array
	 */
	private function recursiveInclude($search)
	{
		$included = [];
		foreach ($search as $path) {
			if (empty($path))
				continue;

			if (is_array($path))
				$included[] = $this->recursiveInclude($path);
			else
				if (strtolower(substr(strrchr($path, '.'), 1)) == 'php') {
					include_once $path;
					$included[] = $path;
				}
		}
		return $included;
	}

	/**
	 * @param $dir
	 * @param array $data
	 * @return array
	 * @throws \Exception
	 */
	private function recursiveSearch($dir, $data = [])
	{
		foreach (scandir($dir) as $item) {
			if (in_array($item, ['.', '..']))
				continue;

			if (is_dir($dir . DIRECTORY_SEPARATOR . $item))
				$data[$item] = $this->recursiveSearch($dir . DIRECTORY_SEPARATOR . $item);
			elseif (file_exists($dir . DIRECTORY_SEPARATOR . $item))
				$data[] = $dir . DIRECTORY_SEPARATOR . $item;
			else
				throw new \Exception('Invalid Load' . $dir . DIRECTORY_SEPARATOR . $item);
		}
		return $data;
	}

	/**
	 * @param $dir
	 * @return bool
	 * @throws \Exception
	 */
	public function autoloadFolder($dir)
	{
		$this->included = array_merge(
			$this->recursiveInclude($this->recursiveSearch($dir)),
			$this->include
		);
		return true;
	}
}