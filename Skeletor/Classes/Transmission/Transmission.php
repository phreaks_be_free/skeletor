<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 12/22/17
 * Time: 6:51 AM
 */

namespace Classes\Transmission;


use Skeletor\Shell\Shell;
use Skeletor\Transmission\Model\MagnetLink;
use \Skeletor\Transmission\Model\Link;
use Skeletor\Transmission\Model\TorrentLink;

class Transmission
{
	protected $exe;
	protected $tmp;
    public function __construct()
    {

    	$this->exe = new Shell();

    }


    /**
     * @param MagnetLink $
     * @return string
     */
    protected function queueMagnetLink(MagnetLink $magnetLink)
    {
    	$test = '/usr/bin/transmission-remote -a "' . $magnetLink->run() . '"';
        return shell_exec('/usr/bin/transmission-remote -a "' . $magnetLink->run() . '"');
    }


    protected function queueTorrentLink(TorrentLink $torrentLink){

    	$name = 'tl.'.(new \DateTime('now'))->format('Y-m-d').rand(0,9999999).'.torrent';

    	$test = "wget '".substr($torrentLink->run(), 0, strpos($torrentLink->run(), "?title="))."' '/var/www/htdocs/tmp/".$name."'";

    	$result = $this->exe->submitCommand(
			$test
		)->shell();
	}

	/**
	 * @param Link $link
	 * @return bool|string
	 */
    public function queue(Link $link){
		if(is_a($link,'Skeletor\Transmission\Model\MagnetLink'))
			return $this->queueMagnetLink($link);

		if(is_a($link,'Skeletor\Transmission\Model\TorrentLink'))
			return $this->queueTorrentLink($link);

		return false;
	}
}