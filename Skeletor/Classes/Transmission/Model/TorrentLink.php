<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 12/24/17
 * Time: 5:55 PM
 */
namespace Skeletor\Transmission\Model;
class TorrentLink extends Link
{
	/**
	 * TorrentLink constructor.
	 * @param $torrentLink
	 * @throws \Exception
	 */
	public function __construct($torrentLink)
	{
		$this->defaultLinkType = 0;
		$link = preg_replace( "/\r|\n/", "", $torrentLink );
		parent::__construct($link);
	}

	/**
	 * @return string
	 */
	public function run(){
		return $this->link;
	}
}