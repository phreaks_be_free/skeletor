<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 1/8/18
 * Time: 10:03 PM
 */

namespace Skeletor\Transmission\Model;


abstract class Link
{
	protected $link;
	protected $defaultLinkType;
	protected $defaultLinkTypeOptions = [
		'string'
	];

	/**
	 * @return mixed
	 */
	abstract public function run();

	/**
	 * MagnetLink constructor.
	 * @param $link
	 * @throws \Exception
	 */
	public function __construct($link)
	{

		$this->load($link);

		/**
		 * Check Default Types
		 */
		if(isset($this->defaultLinkType)&&!empty($this->defaultLinkType)
			&& isset($this->defaultLinkTypeOptions[$this->defaultLinkType])){

			if ($this->defaultLinkTypeOptions[$this->defaultLinkType]=='string'&&!is_string($link))
				throw new \Exception('Invalid Link');
		}

	}

	/**
	 * @param $link
	 */
	protected function load($link)
	{
		$this->link = $link;
	}


}