<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 12/24/17
 * Time: 5:55 PM
 */
namespace Skeletor\Transmission\Model;

class MagnetLink extends Link
{
	/**
	 * MagnetLink constructor.
	 * @param $MagnetLink
	 * @throws \Exception
	 */
	public function __construct($MagnetLink)
	{
		$this->defaultLinkType = 0;

		parent::__construct($MagnetLink);
	}

	/**
	 * @return string
	 */
	public function run(){
		return $this->link;
	}
}