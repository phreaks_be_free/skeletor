<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 12/22/17
 * Time: 6:53 AM
 */

namespace Classes\AutoPyTorrent;


use Skeletor\Transmission\Model\MagnetLink;
use Skeletor\Shell\Shell;
use Skeletor\Transmission\Model\TorrentLink;
use Skeletor\Vendor\AutoPyTorrent\Model\Search;

class AutoPyTorrent
{
	public function __construct()
	{

	}

	/**
	 * @param $q
	 * @param bool $pirateSearch
	 * @return bool|MagnetLink|TorrentLink
	 */
	public function search($q, $pirateSearch = true)
	{

		$search = new Search($q, (($pirateSearch) ? 1 : 4));


		$result = (new Shell("/var/www/python/auto_py_torrent.py " . $search->getSource() . " '" . str_replace('"', "", str_replace("'", "", $search->getSearch())) . "'"))
			->shell();

		switch ($result) {
			case 'No torrents found.':
				return false;
		}

		if (strpos($result, '.torrent') > 0)
			return new TorrentLink($result);

		return new MagnetLink($result);
	}


}