<?php
/**
 * Created by PhpStorm.
 * User: jfoy
 * Date: 12/24/17
 * Time: 5:54 PM
 */

namespace Skeletor\Vendor\AutoPyTorrent\Model;

class Search
{
    private $search = false;

    private $sourceSelect = false;

    private $source = [1 => 'piratebay', 4 => 'limetorrents'];

    /**
     * Search constructor.
     * @param null $q
     * @throws \Exception
     */
    public function __construct($q = null, $sourceKey)
    {
        if (!is_string($q))
            throw new \Exception('Invalid Search');

        if (!isset($this->source[$sourceKey]))
            throw new \Exception('Invalid Source');

        $this->populate($q, $sourceKey);
    }

    /**
     * @param $sourceKey
     * @return $this
     * @throws \Exception
     */
    public function changeSource($sourceKey)
    {

        if (!isset($this->source[$sourceKey]))
            throw new \Exception('Invalid SourceKey');

        $this->sourceSelect = $sourceKey;
        return $this;
    }

    /**
     * @param $q
     * @param $sourceKey
     * @return $this
     * @throws \Exception
     */
    public function populate($q, $sourceKey)
    {
        if (!is_string($q))
            throw new \Exception('Invalid Search Query');

        $this->search = $q;

        if (!isset($this->source[$sourceKey]))
            throw new \Exception('Invalid SourceKey');

        $this->sourceSelect = $sourceKey;
        return $this;
    }

    /**
     * @return bool
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * @return bool|mixed
     */
    public function getSource()
    {
        if (!$this->sourceSelect)
            return false;

        switch ($this->source[$this->sourceSelect]) {
            case 'piratebay':
                return '0 1';
                break;

            case 'limetorrents':
                return '0 4';
                break;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getSources()
    {
        return $this->source;
    }
}